from django.conf.urls import url

from lirc_pi.controller import views

urlpatterns = [
    url(r'^$', views.TVRemoconView.as_view(), name='tv_remocon'),
]
