import subprocess

from django.conf import settings
from django.http import JsonResponse
from django.views.generic import TemplateView


class RemoconViewBase(TemplateView):
    btn_prefix = ''
    remocon_name = ''

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if not settings.DEBUG and \
           self.get_remocon_name() not in self.get_remocon_info():
            raise ValueError

    def get_remocon_name(self):
        return self.remocon_name

    def issue_command(self, *args):
        return subprocess.run(
            ['irsend', *args],
            stdout=subprocess.PIPE
        )

    def get_remocon_info(self, name='', btn=''):
        return self.issue_command(
            'LIST', name, btn
        ).stdout.decode().rstrip().split('\n')

    def get_button_name(self):
        return list(map(
            lambda x: x.split()[1],
            self.get_remocon_info(self.get_remocon_name())
        ))

    def send_once(self, btn, count=1):
        return self.issue_command(
            '--count=%d' % count,
            'SEND_ONCE',
            self.get_remocon_name(),
            btn,
        )

    def post(self, request, *args, **kwargs):
        btn = request.POST.get('name', '')

        if hasattr(self, self.btn_prefix + btn):
            getattr(self, self.btn_prefix + btn)(btn)
        elif btn in self.get_button_name():
            self.send_once(btn)
        return JsonResponse({})


class TVRemoconView(RemoconViewBase):
    remocon_name = 'tv'
    template_name = 'controller/remocon.html'

    def on(self, btn):
        """
        when AQUOS is off for a while,
        power signal should be sent more than 1 time.
        """
        self.send_once('power', count=3)

    def off(self, btn):
        self.send_once('power')
